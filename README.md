# Cloud Assignments
List of all my homework assignments for cloud computing.

## Connection Guide
* Just `ssh` into the server.
    * Remember to get the username right.
```ssh
ssh -i C:\Users\slim1\.ssh\stan.pem ubuntu@130.245.170.221
```

## Other Important Info
* Use `sudo` for many things as the vm will block most requests
* To update the **ngnix config**, run this:
    ```sh
    sudo vim /etc/nginx/sites-available/default
    ```
* To fix stubborn dirs from not allowing you to `scp`, change owner to user (ubuntu):
    ```sh
    sudo chown ubuntu /var/www/html 
    ```
* Do not forget to restart nginx with every new change:
    ```sh
    sudo systemctl restart nginx
    ```
* For copying files, don't forget to specify the key:
    ```sh
    scp -i C:\Users\slim1\.ssh\stan.pem -r ./ ubuntu@130.245.170.221:/var/www/html
    ```

## References
#### Nginx
* [Installing Nginx Ubuntu 18.04 - DigitalOcean](https://medium.com/@jgefroh/a-guide-to-using-nginx-for-static-websites-d96a9d034940)
* [A guide to hosting static websites using NGINX](https://medium.com/@jgefroh/a-guide-to-using-nginx-for-static-websites-d96a9d034940)